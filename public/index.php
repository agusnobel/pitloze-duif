<?php

require __DIR__ . '/../vendor/autoload.php';

$database = [
    "schema" => "pitloze_duif",
    "host" => "localhost",
    "username" => "root",
    "password" => ""
];

\AgusNobel\PitlozeDuif\App::start(compact('database'));
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pitloze Duif</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="libs/bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="libs/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<div id="wrapper">
    <div id="main">
        <h1>Duivenmelkers</h1>
        <?php
        foreach (\AgusNobel\PitlozeDuif\Models\Keeper::getAll() as $keeper) {
            ?>
            <h2><?php echo htmlspecialchars(sprintf("%s %s", $keeper->getFirstName(), $keeper->getLastName())) ?></h2>
            <table class="table">
                <tr>
                    <th>Naam</th>
                    <th>Gewicht (gram)</th>
                    <th>Geboorte maand/jaar</th>
                </tr>
            <?php
            foreach ($keeper->getPigeons() as $pigeon) {
                ?>
                <tr>
                    <td><?php echo htmlspecialchars($pigeon->getName()) ?></td>
                    <td><?php echo htmlspecialchars($pigeon->getWeight()) ?></td>
                    <td><?php echo htmlspecialchars(date("m/Y", strtotime($pigeon->getBirthDate()))) ?></td>
                </tr>
                <?php
            }
            ?>
            </table>
            <?php
        }
        ?>
    </div>
</div>

<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="libs/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/vars.js"></script>
<script src="js/main.js"></script>

</body>
</html>
