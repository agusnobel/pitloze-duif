-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Versie:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Databasestructuur van pitloze_duif wordt geschreven
CREATE DATABASE IF NOT EXISTS `pitloze_duif` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `pitloze_duif`;

-- Structuur van  tabel pitloze_duif.keeper wordt geschreven
CREATE TABLE IF NOT EXISTS `keeper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` tinytext NOT NULL,
  `last_name` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel pitloze_duif.keeper: 4 rows
/*!40000 ALTER TABLE `keeper` DISABLE KEYS */;
INSERT INTO `keeper` (`id`, `first_name`, `last_name`) VALUES
	(1, 'Karel', 'de Vries'),
	(2, 'Nicole', 'Winters'),
	(3, 'Peter', 'Laantjes'),
	(4, 'Jeroen', 'Benners');
/*!40000 ALTER TABLE `keeper` ENABLE KEYS */;

-- Structuur van  tabel pitloze_duif.pigeon wordt geschreven
CREATE TABLE IF NOT EXISTS `pigeon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `weight` int(11) NOT NULL,
  `birth_date` datetime DEFAULT NULL,
  `keeper_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `keeper_id` (`keeper_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel pitloze_duif.pigeon: 5 rows
/*!40000 ALTER TABLE `pigeon` DISABLE KEYS */;
INSERT INTO `pigeon` (`id`, `name`, `weight`, `birth_date`, `keeper_id`) VALUES
	(1, 'Speedy', 900, '2017-01-24 00:00:00', 1),
	(2, 'Flying Dutchman', 850, '2017-06-24 00:00:00', 2),
	(3, 'The Flash', 789, '2017-07-24 00:00:00', 3),
	(4, 'Jeroen Junior', 1100, '2016-11-24 00:00:00', 4),
	(5, 'Prince', 1025, '2016-12-24 00:00:00', 1);
/*!40000 ALTER TABLE `pigeon` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
