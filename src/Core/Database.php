<?php

namespace AgusNobel\PitlozeDuif\Core;

class Database
{
    private static $_instance;
    private $_configuration;
    private $_connection;

    /**
     * @param array $configuration
     * @return Database
     */
    public static function getInstance($configuration)
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self($configuration);
        }

        return self::$_instance;
    }

    /**
     * Database constructor.
     * @param array $configuration
     */
    protected function __construct($configuration)
    {
        $this->_configuration = $configuration;

        $this->_connection = new \PDO(
            sprintf("mysql:dbname=%s;host=%s", $configuration["schema"], $configuration["host"]),
            $configuration["username"],
            $configuration["password"]
        );
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->_connection;
    }

    /**
     * @param string $statement
     * @param array $parameters
     * @return \PDOStatement
     */
    public function execute($statement, array $parameters = array())
    {
        $statement = $this->_connection->prepare($statement);
        $statement->execute($parameters);
        return $statement;
    }
}