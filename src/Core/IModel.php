<?php

namespace AgusNobel\PitlozeDuif\Core;

interface IModel
{
    public function update();
}