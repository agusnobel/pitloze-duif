<?php

namespace AgusNobel\PitlozeDuif;

use AgusNobel\PitlozeDuif\Core\Database;

abstract class App
{
    private static $_database;

    public static function start($configuration)
    {
        self::$_database = Database::getInstance($configuration["database"]);
    }

    /**
     * @return Database
     */
    public static function getDatabase()
    {
        return self::$_database;
    }
}