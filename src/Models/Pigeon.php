<?php

namespace AgusNobel\PitlozeDuif\Models;

use AgusNobel\PitlozeDuif\App;
use AgusNobel\PitlozeDuif\Core\IModel;

class Pigeon implements IModel
{
    private $_id;
    private $_name;
    private $_weight;
    private $_birthDate;
    private $_keeperId;

    public function __construct($id, $name, $weight, $birthDate, $keeperId)
    {
        $this->_id = intval($id);
        $this->_name = $name;
        $this->_weight = intval($weight);
        $this->_birthDate = $birthDate;
        $this->_keeperId = intval($keeperId);
    }

    /**
     * @param $keeperId
     * @return Pigeon[]
     */
    public static function findAllByKeeperId($keeperId)
    {
        $statement = App::getDatabase()->execute(
            sprintf("SELECT * FROM pigeon WHERE keeper_id = ?"),
            [
                $keeperId
            ]
        );

        $pigeons = [];

        while ($record = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $pigeons[] = new Pigeon($record["id"], $record["name"], $record["weight"], $record["birth_date"], $record["keeper_id"]);
        }

        return $pigeons;
    }

    public function getId()
    {
        return intval($this->_id);
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getWeight()
    {
        return intval($this->_weight);
    }

    public function setWeight($weight)
    {
        $this->_weight = intval($weight);
    }

    public function getBirthDate()
    {
        return $this->_birthDate;
    }

    public function setBirthDate($birthDate)
    {
        $this->_birthDate = date("Y-m-d H:i:s", strtotime($birthDate));
    }

    public function getKeeperId()
    {
        return intval($this->_keeperId);
    }

    public function setKeeperId($keeperId)
    {
        $this->_keeperId = intval($keeperId);
    }

    public function update()
    {
        App::getDatabase()->execute(
            "UPDATE pigeon SET `name` = ?, weight = ?, birth_date = ? WHERE id = ?",
            [
                $this->_name,
                $this->_weight,
                date("Y-m-d H:i:s", strtotime($this->_birthDate)),
                $this->_id
            ]
        );
    }
}