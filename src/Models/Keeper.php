<?php

namespace AgusNobel\PitlozeDuif\Models;

use AgusNobel\PitlozeDuif\App;
use AgusNobel\PitlozeDuif\Core\IModel;

class Keeper implements IModel
{
    private $_id;
    private $_firstName;
    private $_lastName;

    public function __construct($id, $firstName, $lastName)
    {
        $this->_id = intval($id);
        $this->_firstName = $firstName;
        $this->_lastName = $lastName;
    }

    /**
     * @param null $limit
     * @return Keeper[]
     */
    public static function getAll($limit = null)
    {
        if ($limit !== null && is_numeric($limit)) {
            $limit = sprintf("LIMIT %s", intval($limit));
        } else {
            $limit = "";
        }

        $statement = App::getDatabase()->execute(
            sprintf("SELECT * FROM keeper %s", $limit)
        );

        $keepers = [];

        while ($record = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $keepers[] = new Keeper($record["id"], $record["first_name"], $record["last_name"]);
        }

        return $keepers;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function setFirstName($firstName)
    {
        $this->_firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }

    public function setLastName($lastName)
    {
        $this->_lastName = $lastName;
    }

    /**
     * @return Pigeon[]
     */
    public function getPigeons()
    {
        return Pigeon::findAllByKeeperId($this->getId());
    }

    public function update()
    {
        App::getDatabase()->execute(
            "UPDATE keeper SET first_name = ?, last_name = ? WHERE id = ?",
            [
                $this->_firstName,
                $this->_lastName,
                $this->_id
            ]
        );
    }
}